package pp;
import java.util.*;
public class OOPMatrices extends theMatrix{
	static Scanner input=new Scanner(System.in);
	
	 void MainMenu(){
		 OOPMatrices call = new OOPMatrices();
		while(true){
			
		
			int select;
			System.out.println("\t\t   MATRIX");
			System.out.println("===========================================");			
			System.out.println("1. Addition");
			System.out.println("2. Subtraction");
			System.out.println("3. Multiplication");
			System.out.println("0. Exit");
			System.out.println("===========================================");	
			System.out.println("Please select a number");
			do{
				System.out.print(">> ");
				try{
					select=input.nextInt();
					if(select!= 1 && select!= 2 && select!= 3  && select!= 0 ){
						System.out.println("Please select only the '1', '2', '3', or '0' number");
						continue;
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please select only the '1', '2', '3', or '0' number");
					input.nextLine();
					continue;
				}
			}while(true);
			
			if(select== 0){
				break;
			}
			
			switch(select){
				case 1:
					call.addition();
					break;
				case 2:
					call.substraction();
					break;
				case 3:
					call.multiplication();
					break;
				
				
			}
			
		}
		
		
	}
	
	 void addition(){
		 
		int rows,columns;
		do{
			System.out.println("\t\t  Addition");
			System.out.println("===========================================");
				
			do{	
				try{
					
					System.out.println("Input the number of rows of the matrices ");
					System.out.print(">> ");
					rows=input.nextInt();
					
						if (rows <1){
							throw new InputMismatchException();
						}
					System.out.println("Input the number of columns of the matrices ");
					System.out.print(">> ");
					columns=input.nextInt();
					
						if (columns <1){
							throw new InputMismatchException();
						}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please input a natural number ( > 0 )");
					input.nextLine();
					continue;
				}
			}while(true);
			
			double[][]matrix1= new double[rows][columns];
			double[][]matrix2= new double[rows][columns];
			double[][]matrixResult=new double[rows][columns];
			System.out.println("\t\t MATRIX 1");
			do{
				try{
					for(int rowindex=0; rowindex<rows;rowindex++){
						for(int columnindex=0; columnindex<columns;columnindex++){
							System.out.println("Input the value of row "+(rowindex+1)+" and column "+(columnindex+1));
							System.out.print(">> ");
							matrix1[rowindex][columnindex]=input.nextDouble();
							
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please input a real number");
					input.nextLine();
					continue;
				}	
			}while(true);
			
			System.out.println();
			for(int rowindex=0; rowindex<rows;rowindex++){
				for(int columnindex=0; columnindex<columns;columnindex++){
					
					System.out.printf("%.2f  ",matrix1[rowindex][columnindex]);
				}System.out.println();
			}
			
			
			System.out.println();
			
			System.out.println("\t\t MATRIX 2");
			do{
				try{
					for(int rowindex=0; rowindex<rows;rowindex++){
						for(int columnindex=0; columnindex<columns;columnindex++){
							System.out.println("Please input the value of row "+(rowindex+1)+" and column "+(columnindex+1));
							System.out.print(">> ");
							matrix2[rowindex][columnindex]=input.nextDouble();
							
							matrixResult[rowindex][columnindex]=matrix1[rowindex][columnindex] + matrix2[rowindex][columnindex];
							
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please input a real number");
					input.nextLine();
					continue;
				}	
			}while(true);
			
			System.out.println();
			for(int rowindex=0; rowindex<rows;rowindex++){
				for(int columnindex=0; columnindex<columns;columnindex++){
					
					System.out.printf("%.2f  ",matrix2[rowindex][columnindex]);
				}System.out.println();
			}
			
			System.out.println("\n");
			System.out.println("The result of the addition of both matrices is ");
			System.out.println();
			for(int rowindex=0; rowindex<rows;rowindex++){
				for(int columnindex=0; columnindex<columns;columnindex++){
					this.matrix1=matrix1[rowindex][columnindex];
					this.matrix2=matrix2[rowindex][columnindex];
					
					System.out.printf("%.2f",add());
				}System.out.println();
			}
			
			
			
			if(rows != columns){
				System.out.println("The result of the matrix doesn't have a determinant");
			}	
			else{
				
				if(rows==2 && columns==2){
					if(((matrixResult[0][0]*matrixResult[1][1])-(matrixResult[0][1]*matrixResult[1][0]))!=0){
						System.out.println("The determinant of the matrix is "+((matrixResult[0][0]*matrixResult[1][1])-(matrixResult[0][1]*matrixResult[1][0])));
					}
					else{
						System.out.println("It's a mirrored matrix");
					}
				}
				else if(rows==3 && columns==3){
					if(((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
							-(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
							+(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1]))))!=0){
						System.out.println("The determinant of the matrix is "+((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
								-(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
								+(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1])))));
					}
					else{
						System.out.println("It's a mirrored matrix");
					}
				}
			}
			
			
			
			
			System.out.println();
			String choice;
			input.nextLine();
			do{
				System.out.println("Try again ? (y/n)");
				choice=input.nextLine();
				if (choice.contentEquals("n")){
					 break;
				 }
				 else  if (choice.contentEquals("y")){
					 break;
				 }
				 else{
					 System.out.println("Please input only the letter 'y' or 'n'");
					 continue;
				 }
			}while(true);
			 if (choice.contentEquals("n")){
				 break;
			 }
			 else  if (choice.contentEquals("y")){
				 continue;
			 }
			
		}while(true);
		
		
	}
	
	 void substraction(){
		int rows,columns;
		do{
			System.out.println("\t\t  Subtraction");
			System.out.println("===========================================");
		
			do{	
				try{
					
					System.out.println("Input the number of rows of the matrices ");
					System.out.print(">> ");
					rows=input.nextInt();
					
						if (rows <1){
							throw new InputMismatchException();
						}
					System.out.println("Input the number of columns of the matrices ");
					System.out.print(">> ");
					columns=input.nextInt();
					
						if (columns <1){
							throw new InputMismatchException();
						}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please input a natural number ( > 0 )");
					input.nextLine();
					continue;
				}
			}while(true);
			
			double[][]matrix1= new double[rows][columns];
			double[][]matrix2= new double[rows][columns];
			double[][]matrixResult=new double[rows][columns];
			System.out.println("\t\t MATRIX 1");
			do{
				try{
					for(int rowindex=0; rowindex<rows;rowindex++){
						for(int columnindex=0; columnindex<columns;columnindex++){
							System.out.println("Input the value of row "+(rowindex+1)+" and column "+(columnindex+1));
							System.out.print(">> ");
							matrix1[rowindex][columnindex]=input.nextDouble();
							
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please input a real number");
					input.nextLine();
					continue;
				}	
			}while(true);
			
			System.out.println();
			for(int rowindex=0; rowindex<rows;rowindex++){
				for(int columnindex=0; columnindex<columns;columnindex++){
					
					System.out.printf("%.2f  ",matrix1[rowindex][columnindex]);
				}System.out.println();
			}
			
			
			System.out.println();
			
			System.out.println("\t\t MATRIX 2");
			do{
				try{
					for(int rowindex=0; rowindex<rows;rowindex++){
						for(int columnindex=0; columnindex<columns;columnindex++){
							System.out.println("Please input the value of row "+(rowindex+1)+" and column "+(columnindex+1));
							System.out.print(">> ");
							matrix2[rowindex][columnindex]=input.nextDouble();
							
							matrixResult[rowindex][columnindex]=matrix1[rowindex][columnindex] + matrix2[rowindex][columnindex];
							
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please input a real number");
					input.nextLine();
					continue;
				}	
			}while(true);
			
			System.out.println();
			for(int rowindex=0; rowindex<rows;rowindex++){
				for(int columnindex=0; columnindex<columns;columnindex++){
					
					System.out.printf("%.2f  ",matrix2[rowindex][columnindex]);
				}System.out.println();
			}
			
			System.out.println("\n");
			System.out.println("The result of the substraction of both matrices is ");
			System.out.println();
			for(int rowindex=0; rowindex<rows;rowindex++){
				for(int columnindex=0; columnindex<columns;columnindex++){
					this.matrix1=matrix1[rowindex][columnindex];
					this.matrix2=matrix2[rowindex][columnindex];
					
					System.out.printf("%.2f ",substract());
				}System.out.println();
			}
			
			
			
			if(rows != columns){
				System.out.println("The result of the matrix doesn't have a determinant");
			}	
			else{
				
				if(rows==2 && columns==2){
					if(((matrixResult[0][0]*matrixResult[1][1])-(matrixResult[0][1]*matrixResult[1][0]))!=0){
						System.out.println("The determinant of the matrix is "+((matrixResult[0][0]*matrixResult[1][1])-(matrixResult[0][1]*matrixResult[1][0])));
					}
					else{
						System.out.println("It's a mirrored matrix");
					}
				}
				else if(rows==3 && columns==3){
					if(((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
							-(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
							+(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1]))))!=0){
						System.out.println("The determinant of the matrix is "+((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
								-(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
								+(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1])))));
					}
					else{
						System.out.println("It's a mirrored matrix");
					}
				}
			}
			
			
			
			
			System.out.println();
			String choice;
			input.nextLine();
			do{
				System.out.println("Try again ? (y/n)");
				choice=input.nextLine();
				if (choice.contentEquals("n")){
					 break;
				 }
				 else  if (choice.contentEquals("y")){
					 break;
				 }
				 else{
					 System.out.println("Please input only the letter 'y' or 'n'");
					 continue;
				 }
			}while(true);
			 if (choice.contentEquals("n")){
				 break;
			 }
			 else  if (choice.contentEquals("y")){
				 continue;
			 }
			
		}while(true);
		
		
	}
	
	 void multiplication(){
		
		while(true){
			 int matrixMultiTotal=0,choice=0,rows1,columns1,rows2,columns2;
		 do{
			 do{
				 try{
					 System.out.print("The number of rows of matrix 1: "); //  rows1*columns1   rows1   rows2*columns2
					 rows1=input.nextInt();
					 System.out.print("The number of columns of matrix 1: ");
					 columns1=input.nextInt();
					 System.out.print("The number of rows of matrix 2: ");
					 rows2=input.nextInt();
					 System.out.print("The number of columns of matrix 2: ");
					 columns2=input.nextInt();
					 break;
				 }catch(InputMismatchException e){
					 System.out.println("Please input an integer");
					 input.nextLine();
					 continue;
				 }
			 }while(true);
			 
			
			 
					if (columns1 != rows2) {
						System.out.println("The number of rows of matrix 1 does not equal the number of columns of matrix 2");
						System.out.println("Matrices cannot be multiplied");					
						do{
							try{
								System.out.print("Press '0' to repeat the process or '1' to terminate : ");
								choice=input.nextInt();
								if (choice==1)break;
								if (choice==0)break;
							}catch(InputMismatchException e){
								System.out.println("Input one of the given number");
								input.nextLine();
								continue;
							}
							
						}while((choice!=0)||(choice!=1));
					}			
					if (choice==1)break;
					if (choice==0)continue;
		 }while(columns1!=rows2);
			 if (choice==1)break;
			 System.out.println("\n");
			 
			 
		int[][]matrix=new int[rows1][columns1];
		
		do{
			System.out.println("Matrix 1: ");
			for(int rowindex=0;rowindex<rows1;rowindex++){
				for(int columnindex=0;columnindex<columns1;columnindex++){
					System.out.print("Row "+(rowindex+1)+" column "+(columnindex+1)+" : ");
					try{
						matrix[rowindex][columnindex]=input.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Please input only an integer");
						input.nextLine();
						columnindex--;
						continue;
					}
					
				}
			}
			break;
		}while(true);
		
		
		System.out.println();
		
		for(int rowindex=0;rowindex<rows1;rowindex++){
			for(int columnindex=0;columnindex<columns1;columnindex++){
				System.out.printf("%4d ", matrix[rowindex][columnindex]);
			}System.out.println();
		}System.out.println();
		
		
		
		
		
		
		
		int[][]matrix2=new int[rows2][columns2];
		do{
			System.out.println("Matrix 2: ");
			for(int rowindex=0;rowindex<rows2;rowindex++){
				for(int columnindex=0;columnindex<columns2;columnindex++){
					System.out.print("row "+(rowindex+1)+" column "+(columnindex+1)+" : ");
					try{
						matrix2[rowindex][columnindex]=input.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Please input only an integer");
						input.nextLine();
						columnindex--;
						continue;
					}
					
				}
			}
			break;
		}while(true);
		
		
		
		System.out.println();
		
		
		
		for(int rowindex=0;rowindex<rows2;rowindex++){
			for(int columnindex=0;columnindex<columns2;columnindex++){
				System.out.printf("%4d ", matrix2[rowindex][columnindex]);
			}System.out.println();
		}
		
		
		
		
		
		int[][]matrix3=new int[rows1][columns2];
		for(int rowindex=0;rowindex<rows1;rowindex++){
			for(int columnindex=0;columnindex<columns2;columnindex++){
				matrixMultiTotal=0;
				for(int multiplicationindex=0;multiplicationindex<columns1;multiplicationindex++){
					matrixMultiTotal+=matrix[rowindex][multiplicationindex]*matrix2[multiplicationindex][columnindex];
				}
				matrix3[rowindex][columnindex]=matrixMultiTotal;
				
			}
				}System.out.println();
				
				
			
		System.out.println("The multiplication of matrices "+rows1+"x"+columns1+" X "+rows2+"x"+columns2+" will make up "+rows1+"x"+columns2+" matrix");
		System.out.println("The result of the multiplication of both matrices is :\n");
		for(int rowindex=0;rowindex<rows1;rowindex++){
			for(int columnindex=0;columnindex<columns2;columnindex++){
				this.multiply=matrix3[rowindex][columnindex];
				System.out.printf("%.2f ", multiply());
			}
			System.out.println();
		}
		
		System.out.println("\n");
		
		
		if(rows1 != columns2){
			System.out.println("The result of the matrix doesn't have a determinant");
		}	
		else{
			if(rows1==2 && columns2==2){
				if(((matrix3[0][0]*matrix3[1][1])-(matrix3[0][1]*matrix3[1][0]))!=0){
					System.out.println("The determinant of the matrix is "+((matrix3[0][0]*matrix3[1][1])-(matrix3[0][1]*matrix3[1][0])));
				}
				else{
					System.out.println("It's a mirrored matrix");
				}
			}
			else if(rows1==3 && columns2==3){
				if(((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
						-(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
						+(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1]))))!=0){
					
					System.out.println("The determinant of the matrix is "+((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
							-(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
							+(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1])))));
				}
				else{
					System.out.println("It's a mirrored matrix");
				}
			}
		}
			
			
			do{
				try{
					System.out.print("Press '1' to repeat the process or '0' to terminate");
					choice=input.nextInt();
					if (choice==0)break;
					if (choice==1)break;
				}catch(InputMismatchException e){
					System.out.println("Input one of the given number");
					input.nextLine();
					continue;
				}
				
			}while((choice<0)||(choice>1));
			
			if (choice==0)break;
			if(choice==1)continue;
			System.out.println();
		 }
	}
	
	public static void main(String[] args){
		OOPMatrices call = new OOPMatrices();
		try{
			call.MainMenu();
		}catch(NoSuchElementException e){
			System.out.println("\n");
			System.out.println("You pressed Ctrl + Z");
			System.out.println("Program terminated");
		}		
		//nothing else to see here
    }	
}
class theMatrix{
	double matrix1,matrix2,multiply;
		double add(){
			return matrix1+matrix2;
	}
		double substract(){
			return matrix1-matrix2;
		}
		double multiply(){
			return multiply;
		}
	

}