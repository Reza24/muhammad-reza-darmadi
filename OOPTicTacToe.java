package pp;
import java.util.*;
class board{
	char[][]tictactoe=new char[3][3];
	
	
	void display(){
		
		System.out.println("\n");
		System.out.println("\t  "+tictactoe[0][0]+" |   "+tictactoe[0][1]+"   | "+tictactoe[0][2]);
		System.out.println("\t____|_______|____");
		System.out.println("\t  "+tictactoe[1][0]+" |   "+tictactoe[1][1]+"   | "+tictactoe[1][2]);
		System.out.println("\t____|_______|____");
		System.out.println("\t  "+tictactoe[2][0]+" |   "+tictactoe[2][1]+"   | "+tictactoe[2][2]);
		System.out.println("\t    |       |    ");
		System.out.println("\n");
	}
	
}
public class OOPTicTacToe extends board{
	
	String choice2,choice;
	int number,player=1,select;
	static Scanner input=new Scanner(System.in);
	
	
	
	
	void play(){
		char[][]tictactoe=new char[3][3];
		
		OOPTicTacToe call = new OOPTicTacToe();
			for(int row=0;row<3;row++){
				for(int col=0;col<3;col++){
					tictactoe[row][col]=' ';
					this.tictactoe[row][col]=tictactoe[row][col];
				}
			}
			
			
			do{
				do{
					
					System.out.println("===================================");
					System.out.println("\n");
					
					System.out.println("Choose your mark ( x / o )");
					try{
						choice=input.nextLine();
						if(choice.contentEquals("x") == false && choice.contentEquals("o") == false){
							throw new InputMismatchException();
						}
					}catch(InputMismatchException e){
						System.out.println("-----------------------------------");
						System.out.println("Please input only the 'x' or 'o' letter");
					}
					
						//this block determines whether to write 'x' or 'o'
						if(choice.contentEquals("x")){
							number=1;
							break;
						}
						if(choice.contentEquals("o")){
							number=0;
							break;
						}
						////
						
				}while(true);
				
				while(true){
					int threshold=0;
					System.out.println("===================================");
	
					
					System.out.println("\n");
					System.out.println("\t  "+tictactoe[0][0]+" |   "+tictactoe[0][1]+"   | "+tictactoe[0][2]);
					System.out.println("\t____|_______|____");
					System.out.println("\t  "+tictactoe[1][0]+" |   "+tictactoe[1][1]+"   | "+tictactoe[1][2]);
					System.out.println("\t____|_______|____");
					System.out.println("\t  "+tictactoe[2][0]+" |   "+tictactoe[2][1]+"   | "+tictactoe[2][2]);
					System.out.println("\t    |       |    ");
					System.out.println("\n");
				
					if(tictactoe[0][0]!=' ' && tictactoe[0][0]==tictactoe[0][1] && tictactoe[0][0]==tictactoe[0][2]||
						tictactoe[1][0]!=' ' && tictactoe[1][0]==tictactoe[1][1] && tictactoe[1][0]==tictactoe[1][2]||
						tictactoe[2][0]!=' ' && tictactoe[2][0]==tictactoe[2][1] && tictactoe[2][0]==tictactoe[2][2]||
						tictactoe[0][0]!=' ' && tictactoe[0][0]==tictactoe[1][1] && tictactoe[0][0]==tictactoe[2][2]||
						tictactoe[0][2]!=' ' && tictactoe[0][2]==tictactoe[1][1] && tictactoe[0][2]==tictactoe[2][0]||
						tictactoe[0][0]!=' ' && tictactoe[0][0]==tictactoe[1][0] && tictactoe[0][0]==tictactoe[2][0]||
						tictactoe[0][1]!=' ' && tictactoe[0][1]==tictactoe[1][1] && tictactoe[0][1]==tictactoe[2][1]||
						tictactoe[0][2]!=' ' && tictactoe[0][2]==tictactoe[1][2] && tictactoe[0][2]==tictactoe[2][2])
					{
						if(player==1)player++;
						else if(player==2)player--;
						System.out.println("GAME OVER, PLAYER "+player+" WINS");
						do{
							try{
								
								System.out.println("Play again? (y/n)");
								choice2=input.nextLine();
								
								if(choice2.contentEquals("y") == false && choice2.contentEquals("n") == false){
									throw new InputMismatchException();
								}
							}
							catch(InputMismatchException e){
								System.out.println("------------------------------");
								System.out.println("Please input only the 'y' or 'n' letter");
								
								continue;
							}break;
						}while(true);
						
						break;
					}
					
					
					for(int row=0;row<3;row++){
						for(int col=0;col<3;col++){
							if(tictactoe[row][col]!=' '){
								threshold++;
							}
						}
					}if (threshold==9){
						System.out.println("GAME OVER, PLAYERS HAVE TIED");
						do{
							try{
								
								System.out.println("Play again? (y/n)");
								choice2=input.nextLine();
								
								if(choice2.contentEquals("y") == false && choice2.contentEquals("n") == false){
									throw new InputMismatchException();
								}
							}
							catch(InputMismatchException e){
								System.out.println("------------------------------");
								System.out.println("Please input only the 'y' or 'n' letter");
								
								continue;
							}break;
						}while(true);
						if(choice2.contentEquals("n")){
							break;
						}else if(choice2.contentEquals("y")){
							break;
						}
					}
					
					System.out.println("\tPlayer "+player+"'s turn");
					System.out.println();
					System.out.println("Input one of the following number to fill the board :");
					if(tictactoe[0][0]==' '){
						System.out.println("1. Row 1 Column 1");
					}
					if(tictactoe[0][1]==' '){
						System.out.println("2. Row 1 Column 2");
					}
					if(tictactoe[0][2]==' '){
						System.out.println("3. Row 1 Column 3");
					}
					if(tictactoe[1][0]==' '){
						System.out.println("4. Row 2 Column 1");
					}
					if(tictactoe[1][1]==' '){
						System.out.println("5. Row 2 Column 2");
					}
					if(tictactoe[1][2]==' '){
						System.out.println("6. Row 2 Column 3");
					}
					if(tictactoe[2][0]==' '){
						System.out.println("7. Row 3 Column 1");
					}
					if(tictactoe[2][1]==' '){
						System.out.println("8. Row 3 Column 2");
					}
					if(tictactoe[2][2]==' '){
						System.out.println("9. Row 3 Column 3");
					}
					do{
						
						System.out.print(">> ");
						select=-1;
						try{
							select=input.nextInt();
							input.nextLine();
							break;
						}catch(InputMismatchException e){
							System.out.println("Please input only the given number");
							input.nextLine();
							select=-1;
							continue;
						}
					}while(true);
					
					switch(select){
					case 1:
						if(tictactoe[0][0]==' ' && select==1){
							if(number==1){
								tictactoe[0][0]='x';
								this.tictactoe[0][0]=tictactoe[0][0];
								number--;
							}
							else if(number==0){
								tictactoe[0][0]='o';
								this.tictactoe[0][0]=tictactoe[0][0];
								number++;
							}
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(tictactoe[0][0]!=' ' && select==1){
							System.out.println("Please fill one of the empty space");
						}break;
					case 2:
						if(tictactoe[0][1]==' ' && select==2){
							if(number==1){
								tictactoe[0][1]='x';
								this.tictactoe[0][1]=tictactoe[0][1];
								number--;
							}
							else if(number==0){
								tictactoe[0][1]='o';
								this.tictactoe[0][1]=tictactoe[0][1];
								number++;
							}
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(tictactoe[0][1]!=' ' && select==2){
							System.out.println("Please fill one of the empty space");
						}break;
					case 3:
						if(tictactoe[0][2]==' ' && select==3){
							if(number==1){
								tictactoe[0][2]='x';
								this.tictactoe[0][2]=tictactoe[0][2];
								number--;
							}
							else if(number==0){
								tictactoe[0][2]='o';
								this.tictactoe[0][2]=tictactoe[0][2];
								number++;
							}	
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(tictactoe[0][2]!=' ' && select==3){
							System.out.println("Please fill one of the empty space");
						}
					case 4:
						if(tictactoe[1][0]==' ' && select==4){
							if(number==1){
								tictactoe[1][0]='x';
								this.tictactoe[1][0]=tictactoe[1][0];
								number--;
							}
							else if(number==0){
								tictactoe[1][0]='o';
								this.tictactoe[1][0]=tictactoe[1][0];
								number++;
							}	
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(tictactoe[1][0]!=' ' && select==4){
							System.out.println("Please fill one of the empty space");
						}break;
					case 5:
						if(tictactoe[1][1]==' ' && select==5){
							if(number==1){
								tictactoe[1][1]='x';
								this.tictactoe[1][1]=tictactoe[1][1];
								number--;
							}
							else if(number==0){
								tictactoe[1][1]='o';
								this.tictactoe[1][1]=tictactoe[1][1];
								number++;
							}	
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(tictactoe[1][1]!=' ' && select==5){
							System.out.println("Please fill one of the empty space");
						}break;
					case 6:
						if(tictactoe[1][2]==' ' && select==6){
							if(number==1){
								tictactoe[1][2]='x';
								this.tictactoe[1][2]=tictactoe[1][2];
								number--;
							}
							else if(number==0){
								tictactoe[1][2]='o';
								this.tictactoe[1][2]=tictactoe[1][2];
								number++;
							}	
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(tictactoe[1][2]!=' ' && select==6){
							System.out.println("Please fill one of the empty space");
						}break;
					case 7:
						if(tictactoe[2][0]==' ' && select==7){
							if(number==1){
								tictactoe[2][0]='x';
								this.tictactoe[2][0]=tictactoe[2][0];
								number--;
							}
							else if(number==0){
								tictactoe[2][0]='o';
								this.tictactoe[2][0]=tictactoe[2][0];
								number++;
							}	
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(tictactoe[2][0]!=' ' && select==7){
							System.out.println("Please fill one of the empty space");
						}break;
					case 8:
						if(tictactoe[2][1]==' ' && select==8){
							if(number==1){
								tictactoe[2][1]='x';
								this.tictactoe[2][1]=tictactoe[2][1];
								number--;
							}
							else if(number==0){
								tictactoe[2][1]='o';
								this.tictactoe[2][1]=tictactoe[2][1];
								number++;
							}
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(tictactoe[2][1]!=' ' && select==8){
							System.out.println("Please fill one of the empty space");
						}break;
					case 9:
						if(tictactoe[2][2]==' ' && select==9){
							if(number==1){
								tictactoe[2][2]='x';
								this.tictactoe[2][2]=tictactoe[2][2];
								number--;
							}
							else if(number==0){
								tictactoe[2][2]='o';
								this.tictactoe[2][2]=tictactoe[2][2];
								number++;
							}	
								if(player==1)player++;
								else if(player==2)player--;
						}
						else if(tictactoe[2][2]!=' ' && select==9){
							System.out.println("Please fill one of the empty space");
						}break;
					default:
						System.out.println("Please select a correct number");
						break;
					}				
				}
				
				if(choice2.contentEquals ("y") ){
					continue;
				}
				else if(choice2.contentEquals ("n") ){
					break;
				}
				
			}while(true);
			
		
	}
	
	public static void main(String[] args) {
		OOPTicTacToe call = new OOPTicTacToe();
			try{
				do{
					int select=-1;
					
					System.out.println("\n");
					System.out.println("===========TIC TAC TOE===========");
					System.out.println("1. Play against your friend");
					System.out.println("2. Play against A.I");
					System.out.println("0. Exit");
					System.out.println("Please select a number");
					try{
						select=input.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Please input only the number '1', '2', or '0'");
						input.nextLine();
						continue;
					}catch(NoSuchElementException e){
						System.out.println();
						System.out.println("You pressed Ctrl+Z");
						System.out.println("Program Terminated");
						break;
					}
					
					switch(select){
					case 1:
						input.nextLine();
						call.play();
						break;
					case 2:
						input.nextLine();
						call.play();
						break;
					case 0:
						break;
					default:
					 	System.out.println("\n");
						System.out.println("Please input only the number '1', '2', or '0");
						break;
					}
					if (select==0){
						break;
					}
				}while(true);
			
		}catch(NoSuchElementException e){
			System.out.println("\n");
			System.out.println("You pressed Ctrl+Z");
			System.out.println("Program Terminated");
		}
    }	
}