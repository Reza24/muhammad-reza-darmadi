import java.util.*;
public class dua3dimensi {

	public static void menu(){
		// TODO Auto-generated method stub
		System.out.println("1. Dua_Dimensi");
		System.out.println("2. Tiga_Dimensi");
		System.out.println("3. Keluar");
		System.out.println(" Silahkan pilih operasi perhitungan yang diinginkan ");
	}
	public static void Dua_Dimensi(){
		int pilihan = -1;
		System.out.println("1. Persegi");
		System.out.println("2. Persegi panjang");
		System.out.println("3. Segitiga");
		System.out.println("4. Jajar genjang");
		System.out.println("5. Belah ketupat");
		System.out.println("6. Trapesium");
		System.out.println("7. Lingkaran");
		System.out.println(" Silahkan pilih operasi perhitungan yang diinginkan ");
		Scanner input = new Scanner(System.in);
		
		do{
			try{
				pilihan=input.nextInt();
				continue;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(pilihan<1 || pilihan>7);
		switch (pilihan){
		case 1 :
			Persegi();
			break;
		case 2 :
			PersegiPanjang();
			break;
		case 3 :
			SegitigaSamaKaki();
			break;
		case 4 :
			JajarGenjang();
			break;
		case 5 :
			BelahKetupat();
			break;
		case 6 :
			Trapesium();
			break;
		case 7 :
			Lingkaran();
			break;
		}
	}
	public static void Persegi(){
		Scanner input = new Scanner(System.in);
		System.out.println("Masukkan Panjang Sisi :");
		float sisi;
		do{
			try{
				sisi=input.nextFloat();
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(true);
		double Luas_Persegi=sisi*sisi;
		System.out.println("Luas persegi adalah "+Luas_Persegi);
		double Keliling_Persegi = 4*sisi;
		System.out.println("Keliling persegi adalah "+Keliling_Persegi);
	}
	public static void PersegiPanjang(){
		Scanner input = new Scanner(System.in);
		System.out.println("Masukkan Panjang : ");
		float panjang;
		System.out.println("Masukkan Lebar : ");
		float lebar;
		do{
			try{
				panjang=input.nextFloat();
				lebar=input.nextFloat();
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(true);
		double Luas_Persegi_Panjang=panjang*lebar;
		System.out.println("Luas Persegi Panjang adalah "+Luas_Persegi_Panjang);
		double keliling_persegi_panjang=(2*panjang)+(2*lebar);
		System.out.println("Keliling persegi panjang adalah "+keliling_persegi_panjang);
	}
	public static void SegitigaSamaKaki(){
		Scanner input = new Scanner (System.in);
		System.out.println("Masukkan sisi alas : ");
		float sisialas;
		System.out.println("Masukkan sisi miring : ");
		float sisimiring;
		System.out.println("Masukkan tinggi : ");
		float tinggi;
		do{
			try{
				sisialas=input.nextFloat();
				sisimiring=input.nextFloat();
				tinggi=input.nextFloat();
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(true);
		double Luas_Segitiga=1/2*(sisialas*tinggi);
		System.out.println("Luas Segitiga adalah "+Luas_Segitiga);
		double keliling_Segitiga=sisialas+(2*sisimiring);
		System.out.println("Keliling Segitiga adalah "+keliling_Segitiga);
	}
	public static void JajarGenjang(){
		Scanner input = new Scanner (System.in);
		System.out.println("Masukkan alas : ");
		float panjang;
		System.out.println("Masukkan tinggi : ");
		float tinggi;
		System.out.println("Masukkan sisi miring : ");
		float sisimiring;
		do{
			try{
				panjang=input.nextFloat();
				tinggi=input.nextFloat();
				sisimiring=input.nextFloat();
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(true);
		double Luas_Jajar_Genjang=panjang*tinggi;
		System.out.println("Luas Jajar Genjang adalah "+Luas_Jajar_Genjang);
		double keliling_Jajar_Genjang=(2*panjang)+(2*sisimiring);
		System.out.println("Keliling Jajar Genjang adalah "+keliling_Jajar_Genjang);
	}
	public static void BelahKetupat(){
		Scanner input = new Scanner (System.in);
		System.out.println("Masukkan diagonal 1 : ");
		float diagonal1;
		System.out.println("Masukkan diagonal 2 : ");
		float diagonal2;
		System.out.println("Masukkan sisi :");
		float sisi;
		do{
			try{
				diagonal1=input.nextFloat();
				diagonal2=input.nextFloat();
				sisi=input.nextFloat();
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(true);
		double Luas_Belah_Ketupat=1/2*(diagonal1*diagonal2);
		System.out.println("Luas Persegi Panjang adalah "+Luas_Belah_Ketupat);
		double keliling_Belah_Ketupat= 4*sisi;
		System.out.println("Keliling Persegi Ketupat adalah "+keliling_Belah_Ketupat);
	}
	public static void Trapesium(){
		Scanner input = new Scanner (System.in);
		System.out.println("Masukkan Panjang sisi pertama (sisi sejajar) : alas : ");
		float sisipertama;
		System.out.println("Masukkan panjang sisi kedua (sisi sejajar) : ");
		float sisikedua;
		System.out.println("Masukkan panjang sisi ketiga : ");
		float sisiketiga;
		System.out.println("Masukkan panjang sisi keempat : ");
		float sisikeempat;
		System.out.println("Masukkan tinggi trapesium : ");
		float tinggi;
		do{
			try{
				sisipertama=input.nextFloat();
				sisikedua=input.nextFloat();
				sisiketiga=input.nextFloat();
				sisikeempat=input.nextFloat();
				tinggi=input.nextFloat();
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(true);
		double Luas = 0.5*(sisipertama+sisikedua)*tinggi;
		System.out.println("Luas Trapesium "+Luas);
		double keliling_Trapesium = sisipertama+sisikedua+sisiketiga+sisikeempat;
		System.out.println("Keliling Trapesium adalah "+keliling_Trapesium);
	}
	public static void Lingkaran(){
		Scanner input = new Scanner (System.in);
		System.out.println("Masukkan Panjang Jari-jari Lingkaran : ");
		float jarijari;
		do{
			try{
				jarijari=input.nextFloat();
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(true);
		double Luas = Math.PI*Math.pow(jarijari, 2);
		System.out.println("Luas Lingkaran "+Luas);
		double keliling_Lingkaran = 2*Math.PI*jarijari;
		System.out.println("Keliling Lingkaran adalah "+keliling_Lingkaran);
			}
			public static void Tiga_Dimensi(){
				int pilihan = -1;
				System.out.println("1. Tabung");
				System.out.println("2. Balok");
				System.out.println("3. Kubus");
				System.out.println("4. Kerucut");
				System.out.println("5. Bola");
				System.out.println("6. Limas Segiempat");
				System.out.println(" Silahkan pilih operasi perhitungan yang diinginkan ");
				Scanner input = new Scanner(System.in);
				
				do{
					try{
						pilihan=input.nextInt();
						continue;
					}
					catch(InputMismatchException e){
						System.out.println("Salah, silahkan input sekali lagi");
						input.nextLine();
						continue;
					}
				}while(pilihan<1 || pilihan>7);
				switch (pilihan){
				case 1 :
					Tabung();
					break;
				case 2 :
					Balok();
					break;
				case 3 :
					Kubus();
					break;
				case 4 :
					Kerucut();
					break;
				case 5 :
					Bola();
					break;
				case 6 :
					Limassegiempat();
					break;
				}
				
			}
			public static void Tabung(){
				Scanner input = new Scanner (System.in);
				System.out.println("Masukkan Jari-jari : ");
				float jarijari;
				System.out.println("Masukkan Tinggi Tabung : ");
				float tinggi;
				do{
					try{
						jarijari=input.nextFloat();
						tinggi=input.nextFloat();
						break;
					}
					catch(InputMismatchException e){
						System.out.println("Salah, silahkan input sekali lagi");
						input.nextLine();
						continue;
					}
				}while(true);
				double Luas = 2*Math.PI*jarijari*(jarijari+tinggi);
				System.out.println("Luas Tabung "+Luas);
				double Volume = Math.PI*Math.pow(jarijari,2)*tinggi;
				System.out.println("Volume tabung adalah "+Volume);
			}
				public static void Balok(){
					Scanner input = new Scanner (System.in);
					System.out.println("Masukkan Panjang Balok : ");
					float panjang;
					System.out.println("Masukkan lebar balok : ");
					float lebar;
					System.out.println("Masukkan tinggi balok : ");
					float tinggi;

					do{
						try{
							panjang=input.nextFloat();
							lebar=input.nextFloat();
							tinggi=input.nextFloat();
							break;
						}
						catch(InputMismatchException e){
							System.out.println("Salah, silahkan input sekali lagi");
							input.nextLine();
							continue;
						}
					}while(true);
					double Luas = 2*((panjang*lebar)+(panjang*tinggi)+(lebar+tinggi));
					System.out.println("Luas balok "+Luas);
					double keliling_balok = 4*(panjang+lebar+tinggi);
					System.out.println("keliling permukaan balok adalah "+keliling_balok);
					double Volume = panjang*lebar*tinggi;
					System.out.println("Volume balok adalah "+Volume);
				}
				public static void Kubus(){
					Scanner input = new Scanner (System.in);
					System.out.println("Masukkan Panjang sisi Balok : ");
					float sisi;

					do{
						try{
							sisi=input.nextFloat();
							break;
						}
						catch(InputMismatchException e){
							System.out.println("Salah, silahkan input sekali lagi");
								input.nextLine();
			continue;
						}
					}while(true);
					double Luas = 6*Math.pow(sisi,2);
					System.out.println("Luas permukaan balok"+Luas);
					double keliling = 12*sisi;
					System.out.println("keliling permukaan balok adalah "+keliling);
					double Volume = Math.pow(sisi,3);
					System.out.println("Volume balok adalah "+Volume);
				}
				public static void Kerucut(){
					Scanner input = new Scanner (System.in);
					System.out.println("Masukkan jari - jari : ");
					float jarijari;
					System.out.println("Masukkan tinggi kerucut : ");
					float tinggi;

					do{
						try{
							jarijari=input.nextFloat();
							tinggi=input.nextFloat();
							break;
						}
						catch(InputMismatchException e){
							System.out.println("Salah, silahkan input sekali lagi");
								input.nextLine();
			continue;
						}
					}while(true);
					double Luas = Math.PI*jarijari*(jarijari+Math.sqrt(Math.pow(tinggi,2))+Math.pow(jarijari, 2));
					System.out.println("Luas kerucut"+Luas);
					double Volume = 1/3*Math.PI*Math.pow(jarijari,2)*tinggi;
					System.out.println("Volume kerucut adalah "+Volume);					
				}
				public static void Bola(){
					Scanner input = new Scanner (System.in);
					System.out.println("Masukkan jari - jari : ");
					float jarijari;

					do{
						try{
							jarijari=input.nextFloat();
							break;
						}
						catch(InputMismatchException e){
							System.out.println("Salah, silahkan input sekali lagi");
								input.nextLine();
			continue;
						}
					}while(true);
					double Luas = 4*Math.PI*Math.pow(jarijari,2);
					System.out.println("Luas bola"+Luas);
					double Volume = 3/4*Math.PI*Math.pow(jarijari,3);
					System.out.println("Volume bola adalah "+Volume);	
									
					}
					public static void Limassegiempat(){
						Scanner input = new Scanner (System.in);
						System.out.println("Masukkan Panjang sisi alas : ");
						float sisi;
						System.out.println("Masukkan Tinggi limas : ");
						float tinggi;
						do{
							try{
								sisi=input.nextFloat();
								tinggi=input.nextFloat();
								break;
							}
							catch(InputMismatchException e){
								System.out.println("Salah, silahkan input sekali lagi");
									input.nextLine();
				continue;
							}
						}while(true);
						double Luasalas = Math.pow(sisi,2);
						double tinggisisi = Math.sqrt(Math.pow(0.5*sisi,2));
						double luassisi = 0.5*sisi*tinggisisi;
						double Luas = Luasalas+(4*luassisi);
						System.out.println("Luas Limas Segiempat"+Luas);
						double Keliling = (4*sisi)+(4*Math.sqrt(Math.pow(tinggisisi,2)+Math.pow(0.5*sisi,2)));
						System.out.println("Keliling Limas Segiempat"+Keliling);
						double Volume = 1/3*Luasalas*tinggi;
						System.out.println("Volume Limas Segiempat"+Volume);
						
					}
					public static void main (String[]args){
						int pilihan = -1;
								Scanner input = new Scanner(System.in);
								menu();
								do{
									try{
									pilihan = input.nextInt();
									break;
									}catch(InputMismatchException e){
										System.out.println("Salah, silahkan input sekali lagi");
										input.nextLine();
										continue;
									}
								}while(true);
								switch(pilihan){
								case 1:
									Dua_Dimensi();
									break;
								case 2:
									Tiga_Dimensi();
									break;	
								}
										
			}
		}
