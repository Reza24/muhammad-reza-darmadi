
import java.util.Scanner;
public class kalkulator {

	
		    //membuat 3 buah variabel
		    private int bilangan1;
		    private int bilangan2;
		    private int hasil;
		  
		    //membuat method set dan get
		    public void setBilangan1(int n){ bilangan1 = n; }
		    public int getBilangan1(){ return bilangan1; }
		  
		    public void setBilangan2(int n){ bilangan2 = n; }
		    public int getBilangan2(){ return bilangan2; }
		  
		    //karena kita tidak boleh mengeset hasil dari luar,
		    //maka kita hanya membuat getHasil
		    public int gethasil(){ return hasil; }
		  
		    //membuat langkah proses
		    public void hitungPenjumlahan(){
		        hasil = bilangan1+bilangan2;
		    }
		  
		    public void hitungPengurangan(){
		        hasil = bilangan1-bilangan2;
		    }


		    public static void main(String[] args){
		    
		        //membuat objek baru
		        kalkulator x = new kalkulator();
		        Scanner input = new Scanner(System.in);
		        boolean lanjut = true;
		      
		        //penerapan konsep perulangan while supaya menunya berulang
		        while(lanjut==true){
		            System.out.println("");
		            System.out.println("=========================");
		            System.out.println("Kalkulator");
		            System.out.println("1. Penjumlahan");
		            System.out.println("2. Pengurangan");
		            System.out.println("3. keluar");
		            System.out.println("=========================");
		            System.out.print("Masukkan pilihan anda : ");
		            int pilihan = input.nextInt();
		            System.out.println("");
		          
		            System.out.print("Masukkan bilangan pertama : ");
		            int bil1 = input.nextInt();
		            x.setBilangan1(bil1);
		            System.out.println("");
		          
		            System.out.print("Masukkan bilangan kedua : ");
		            int bil2 = input.nextInt();
		            x.setBilangan2(bil2);
		            System.out.println("");
		          
		            //penerapan konsep percabangan if
		            if(pilihan==1){
		                x.hitungPenjumlahan();
		                System.out.println("Hasil Penjumlahan = "+x.gethasil());
		            }
		            else if(pilihan==2){
		                x.hitungPengurangan();
		                System.out.println("Hasil Pengurangan = "+x.gethasil());
		            }
		            else if(pilihan<1 || pilihan>3)// tanda || artinya adalah "atau"
		                System.out.println("Yang anda masukkan tidak sesuai");
		            else
		                lanjut=false;
		        }
		    }
		}

	


